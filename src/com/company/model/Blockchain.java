package com.company.model;

import java.util.ArrayList;
import java.util.List;

public class Blockchain {

    private List<Block> blockchain;
    private int difficulty;
    private int maxSizeBlockChain;

    public Blockchain(int difficulty, int maxSizeBlockChain) {
        this.difficulty = difficulty;
        this.blockchain = new ArrayList<>();
        this.maxSizeBlockChain = maxSizeBlockChain;
    }

    public synchronized void addBlockToBlockChain(Block newBlock) {

        if (blockchain.isEmpty() && newBlock.getPreviousHash().equals("0")) {
            blockchain.add(newBlock);
            System.out.println(newBlock.getData() + ": added to the blockchain.");
        } else if (newBlock.getPreviousHash().equals(this.blockchain.get(this.getBlockchain().size() - 1).getHash()) && blockchain.size() > 0) {
            this.blockchain.add(newBlock);
            System.out.println(newBlock.getData() + ": added to the blockchain.");
        } else {
            System.out.println(newBlock.getData() + ": was too late and couldn't add to the blockchain.");
        }

    }

    public List<Block> getBlockchain() {
        return blockchain;
    }

    public void setBlockchain(List<Block> blockchain) {
        this.blockchain = blockchain;
    }

    public int getMaxSizeBlockChain() {
        return maxSizeBlockChain;
    }

    public void setMaxSizeBlockChain(int maxSizeBlockChain) {
        this.maxSizeBlockChain = maxSizeBlockChain;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public Boolean isChainValid() {
        Block currentBlock;
        Block previousBlock;
        var hashTarget = new String(new char[difficulty]).replace('\0', '0');

        //loop through blockchain to check hashes:
        for (int i = 1; i < blockchain.size(); i++) {
            currentBlock = blockchain.get(i);
            previousBlock = blockchain.get(i - 1);
            //compare registered hash and calculated hash:
            if (!currentBlock.getHash().equals(currentBlock.calculateBlockHash())) {
                System.out.println("Current Hashes not equal.");
                return false;
            }
            //compare previous hash and registered previous hash
            if (!previousBlock.getHash().equals(currentBlock.getPreviousHash())) {
                System.out.println("Previous Hashes not equal");
                return false;
            }
            //check if hash is solved
            if (!currentBlock.getHash().substring(0, difficulty).equals(hashTarget)) {
                System.out.println("This block hasn't been mined");
                return false;
            }
        }
        return true;
    }


}
